#!/bin/sh

# Define homebrew packages to install
HOMEBREW_PKGS=(
  'node'
  'git'
  'curl'
  'wget'
  'tmux'
  'python'
  'vim --override-system-vi --with-python --with-ruby --with-perl'
  'cmake'
  'caskroom/cask/brew-cask'
)

# Commands to run to confirm packages have been installed
HOMEBREW_PKGS_CHECK=(
  'node'
  'git'
  'curl'
  'wget'
  'tmux'
  'python'
  '' # Do not check vim, since vim is always available on Mac
  'cmake'
  '' # caskroom, we cannot check
)

# Homebrew caskroom casks to install
CASKROOM_PKGS=(
  #'google-chrome'
  #'adobe-creative-cloud'
  #'ampps'
  #'iterm2'
  #'slack'
  #'microsoft-office365'
  #'sophos-anti-virus-home-edition'
)

# Get the user running this command
MY_USER=`logname`

homebrew_install_package () {
  sudo -u $MY_USER -- brew link --overwrite $1
  sudo -u $MY_USER -- brew install $1
}
 
caskroom_install_package () {
  sudo -u $MY_USER -- brew cask install $1
}

print_feedback () {
  echo ''
  echo $1
}

install_homebrew () {
  sudo -u $MY_USER -- ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}

print_feedback 'Hi there! This will little program will install all the basic tools youll need.'

if [ "$EUID" -ne 0 ]; then
  echo "EXIT - Please run as root"
  exit
fi

printf 'Are you sure you want to continue? [Y/n] '
read ans_continue
if [ "$ans_continue" != 'y' ] && [ "$ans_continue" != 'Y' ]; then
  print_feedback 'Okay, nice meeting you!'
  exit 1
fi

# Checking if system is a Mac
platform='unknown'
unamestr=`uname`
if [[ "$unamestr" != 'Darwin' ]]; then
  print_feedback '[WARNING] Not a Mac! This installer is only meant for Mac OS X'
  exit 1
fi

# Check for homebrew
print_feedback 'Checking if homebrew is installed...'
chown -R $MY_USER /usr/local # Setup the right permissions
brew --version &> /dev/null
if [ $? != 0 ]; then
  print_feedback 'Homebrew not installed, installing...'
  install_homebrew
else 
  print_feedback 'Homebrew installed, skipping its install'
fi

# Check all homebrew packages for installment if they're not available, install them
print_feedback 'Checking and installing all homebrew packages...'
for i in "${!HOMEBREW_PKGS[@]}"
do
  FORMULA=${HOMEBREW_PKGS[$i]}
  printf "Checking for package $FORMULA... "

  if [ "${HOMEBREW_PKGS_CHECK[$i]}" != '' ]; then
    echo "${HOMEBREW_PKGS_CHECK[$i]}"
    if hash ${HOMEBREW_PKGS_CHECK[$i]} 2>/dev/null; then
      printf "already available on this system, skipping install.\n"
    else 
      printf "not found. Installing with Homebrew...\n"
      homebrew_install_package $FORMULA
    fi
  else 
    echo 'No check'
    homebrew_install_package $FORMULA
  fi
done

print_feedback 'Starting installation of brew caskroom casks...'

export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/etc/Caskroom"
for i in "${!CASKROOM_PKGS[@]}"
do
  CASK=${CASKROOM_PKGS[$i]}

  caskroom_install_package $CASK
done

# Oh My Zsh -- Disabled for now, this will cause the script to exit 
#print_feedback 'Installing Oh My Zsh...'
#sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

# NPM localized install path
print_feedback 'Fixup the default NPM installation folder (see: https://github.com/glenpike/npm-g_nosudo)...'
sh -c "$(wget https://raw.githubusercontent.com/glenpike/npm-g_nosudo/master/npm-g-nosudo.sh -O -)"

print_feedback 'Setting up some better OS config (faster key repeat, enhanced finder, etc...)'
# Setup some system defaults
# Set keyrepeat really fast
defaults write NSGlobalDomain KeyRepeat -int 0.02

# Show Path bar in Finder
defaults write com.apple.finder ShowPathbar -bool true

# Show Status bar in Finder
defaults write com.apple.finder ShowStatusBar -bool true

# Show the ~/Library folder
chflags nohidden ~/Library

# Change the default screenshot location
defaults write com.apple.screencapture location ~/Screenshots

print_feedback 'All done! Keep in mind that...'
echo ' - Only the main Adobe CC application has been installed, use that to install all Adobe packages'
echo ' - You should login to Slack, were not doing that for you'
echo ' - Any NAS that you should connect at this point? Now is the time!'
echo ' - Some of the system settings we just setup require you to restart, might be a good time to do that first.'
