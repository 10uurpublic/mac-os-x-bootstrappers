
# Install apps

**Install Brew:**

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

**Chown /usr/local**
_Needed for homebrew casks_
sudo chown -R `whoami` /usr/local

**Set default cask appdir location**
export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/etc/Caskroom"

**Install all brew recipies**

 - git
 - curl
 - wget
 - node
 - tmux
 
Vim package needs some special care:

 - brew install python
 - brew install vim --with-python --with-ruby --with-perl
 - brew install cmake (Needed for YouCompleteMe)
 - Might need to setup alias for the brew installed version.

**Install all cask packages _(NOTE: all casks should probably be run with --appdir to prevent symlinks)_**

 - google-chrome
 - adobe-creative-cloud
 - adobe-photoshop-cc
 - adobe-illustrator-cc
 - ampps
 - iterm2
 - slack
 - microsoft-office365
 - sophos-anti-virus-home-edition

**Install Oh My ZSH**

sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

**Install SPF13**

-curl http://j.mp/spf13-vim3 -L -o - | sh-

# Configuration

**Alter NPM configuration with:**

https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md
https://github.com/glenpike/npm-g_nosudo -- Preferable (NOTE: First download, chmod +x, then execute)

**AMPPS**

 - Make sure that AMPPS binaries are correctly loaded into PATH.
 - Create default vhost path
 - Enable PHP 5.5
 - Enable needed PHP extensions (to be researched)

**Set keyrepeat really fast**

defaults write NSGlobalDomain KeyRepeat -int 0.02

**Show Path bar in Finder**

defaults write com.apple.finder ShowPathbar -bool true

**Show Status bar in Finder**

defaults write com.apple.finder ShowStatusBar -bool true

**Show the ~/Library folder**

chflags nohidden ~/Library

**Change the default screenshot location**

defaults write com.apple.screencapture location ~/Screenshots

**[OPTIONAL] Set fontsmoothing to light (http://superuser.com/posts/457352/revisions)**

defaults write -g AppleFontSmoothing -int 1
sudo defaults write -g AppleFontSmoothing -int 1

# setup yeoman 

1. Yo
2. Generator-tienuur-drupal
3. Grunt-tienuur-drupal

# Alter dock

https://github.com/kcrawford/dockutil

1. ./dockutil --remove 'all'
2. ./dockutil --add '/Applications/Google\ Chrome.app'
3. ./dockutil --add '/Applications/Slack.app'
4. ./dockutil --add '/Applications/Adobe\ Photoshop\ CC\ 2015/Adobe\ Photoshop\ CC\ 2015.app/
5. ./dockutil --add '/Applications/Adobe\ Illustrator\ CC\ 2015/Adobe\ Illustrator.app/
6. ./dockutil --add /Volumes/Opslag/Projecten/ --section others

# Should be researched

1. Setup NAS?
2. Check if we can prompt the user to login into Adobe CC.
3. Make sure we prompt the user for every step (might want to do that in advance, so they can leave the script running after giving feedback to the prompts).
